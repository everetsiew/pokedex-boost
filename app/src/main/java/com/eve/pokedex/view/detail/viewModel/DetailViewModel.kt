package com.eve.pokedex.view.detail.viewModel

import androidx.lifecycle.ViewModel
import com.eve.pokedex.data.db.UserData
import com.eve.pokedex.data.network.model.PokemonDetailResponse
import com.eve.pokedex.data.network.model.PokemonListItem
import com.eve.pokedex.data.repo.PokemonRepo
import com.eve.pokedex.view.main.viewModel.MainViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import java.io.IOException

@ExperimentalCoroutinesApi
class DetailViewModel(private val repo: PokemonRepo) : ViewModel() {
    private lateinit var job: Job

    private val _pokemonDetailState = MutableStateFlow<PokemonDetailState>(PokemonDetailState.IDLE)
    val pokemonDetailState: StateFlow<PokemonDetailState> = _pokemonDetailState

    sealed class PokemonDetailState {
        data class SUCCESS(val pokemonDetail: PokemonDetailResponse) : PokemonDetailState()
        object FAILED : PokemonDetailState()
        object LOADING : PokemonDetailState()
        object IDLE : PokemonDetailState()
    }

    fun getPokemonDetail(name: String) {
        _pokemonDetailState.value = PokemonDetailState.LOADING

        job = CoroutineScope(Dispatchers.IO).launch {
            try {
                val pokemonDetail = repo.getPokemonDetail(name)
                _pokemonDetailState.value = PokemonDetailState.SUCCESS(pokemonDetail)
            } catch (e: IOException) {
                _pokemonDetailState.value = PokemonDetailState.FAILED
                e.printStackTrace()
            }
        }
    }

    fun setFavorite(name: String): Boolean {
        return if (UserData.favList.contains(PokemonListItem(name))) {
            UserData.favList.remove(name)
            false
        } else {
            UserData.favList.add(name)
            true
        }
    }

    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }
}