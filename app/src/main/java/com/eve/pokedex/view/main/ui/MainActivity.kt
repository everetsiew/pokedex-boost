package com.eve.pokedex.view.main.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.eve.pokedex.R
import com.eve.pokedex.data.network.model.PokemonListItem
import com.eve.pokedex.data.utils.RecyclerViewOnClick
import com.eve.pokedex.databinding.ActivityMainBinding
import com.eve.pokedex.view.detail.ui.DetailActivity
import com.eve.pokedex.view.favoriteList.ui.FavoriteListActivity
import com.eve.pokedex.view.main.adapter.PokemonAdapter
import com.eve.pokedex.view.main.viewModel.MainViewModel
import com.eve.pokedex.view.main.viewModel.MainViewModelFactory
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.IOException

@ExperimentalCoroutinesApi
class MainActivity : AppCompatActivity(), KodeinAware, RecyclerViewOnClick {
    override val kodein by kodein()
    private val factory: MainViewModelFactory by instance()
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProvider(this, factory).get(MainViewModel::class.java)


        binding.favList.setOnClickListener {
            startActivity(Intent(this, FavoriteListActivity::class.java))
        }

        viewModel.getPokemonList(500)
        lifecycleScope.launchWhenCreated {
            viewModel.pokemonListState.collect {
                when (it) {
                    is MainViewModel.PokemonListState.SUCCESS -> {
                        binding.recyclerView.also { recyclerView ->
                            recyclerView.layoutManager = LinearLayoutManager(this@MainActivity)
                            recyclerView.adapter = PokemonAdapter(it.pokemonList, this@MainActivity)
                        }
                    }
                    is MainViewModel.PokemonListState.FAILED -> {

                    }
                    is MainViewModel.PokemonListState.LOADING -> {

                    }
                    is MainViewModel.PokemonListState.IDLE -> {

                    }
                }
            }
        }
    }

    override fun onRecyclerViewClick(view: View, data: Any) {
        data as PokemonListItem
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra("name", data.name)
        startActivity(intent)
    }
}