package com.eve.pokedex.view.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.eve.pokedex.R
import com.eve.pokedex.data.network.model.PokemonListItem
import com.eve.pokedex.data.utils.RecyclerViewOnClick
import com.eve.pokedex.databinding.RecyclerviewPokemonBinding

class PokemonAdapter(
    private val pokemonList: List<PokemonListItem>,
    private val clickListener: RecyclerViewOnClick
) :
    RecyclerView.Adapter<PokemonAdapter.ViewHolder>() {

    inner class ViewHolder(val recyclerViewPokemonBinding: RecyclerviewPokemonBinding) :
        RecyclerView.ViewHolder(recyclerViewPokemonBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.recyclerview_pokemon, parent, false
        )
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.recyclerViewPokemonBinding.pokemon = pokemonList[position]
        holder.recyclerViewPokemonBinding.root.setOnClickListener {
            clickListener.onRecyclerViewClick(
                holder.recyclerViewPokemonBinding.root,
                pokemonList[position]
            )
        }
    }

    override fun getItemCount() = pokemonList.size
}