package com.eve.pokedex.view.favoriteList.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.eve.pokedex.R
import com.eve.pokedex.data.network.model.PokemonListItem
import com.eve.pokedex.data.utils.RecyclerViewOnClick
import com.eve.pokedex.databinding.RecyclerviewPokemonBinding
import com.eve.pokedex.databinding.RecyclerviewPokemonFavBinding

class FavPokemonAdapter(
    private val pokemonList: List<PokemonListItem>,
    private val clickListener: RecyclerViewOnClick
) :
    RecyclerView.Adapter<FavPokemonAdapter.ViewHolder>() {

    inner class ViewHolder(val recyclerViewPokemonFavBinding: RecyclerviewPokemonFavBinding) :
        RecyclerView.ViewHolder(recyclerViewPokemonFavBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.recyclerview_pokemon_fav, parent, false
        )
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.recyclerViewPokemonFavBinding.pokemon = pokemonList[position]
        holder.recyclerViewPokemonFavBinding.root.setOnClickListener {
            clickListener.onRecyclerViewClick(
                holder.recyclerViewPokemonFavBinding.root,
                pokemonList[position]
            )
        }
    }

    override fun getItemCount() = pokemonList.size
}