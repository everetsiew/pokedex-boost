package com.eve.pokedex.view.main.viewModel

import androidx.lifecycle.ViewModel
import com.eve.pokedex.data.network.model.PokemonListItem
import com.eve.pokedex.data.repo.PokemonRepo
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import java.io.IOException

@ExperimentalCoroutinesApi
class MainViewModel(private val repo: PokemonRepo) : ViewModel() {
    private lateinit var job: Job
    private val _pokemonListState = MutableStateFlow<PokemonListState>(PokemonListState.IDLE)
    val pokemonListState: StateFlow<PokemonListState> = _pokemonListState

    sealed class PokemonListState {
        data class SUCCESS(val pokemonList: List<PokemonListItem>) : PokemonListState()
        object FAILED : PokemonListState()
        object LOADING : PokemonListState()
        object IDLE : PokemonListState()
    }

    fun getPokemonList(limit: Int) {
        _pokemonListState.value = PokemonListState.LOADING
        job = CoroutineScope(Dispatchers.IO).launch {
            try {
                val pokemonList = repo.getPokemonList(limit).results
                _pokemonListState.value = PokemonListState.SUCCESS(pokemonList)
            } catch (e: IOException) {
                _pokemonListState.value = PokemonListState.FAILED
                e.printStackTrace()
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }

}