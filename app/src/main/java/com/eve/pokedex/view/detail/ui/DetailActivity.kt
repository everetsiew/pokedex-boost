package com.eve.pokedex.view.detail.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.ahmadrosid.svgloader.SvgLoader
import com.eve.pokedex.R
import com.eve.pokedex.data.db.UserData
import com.eve.pokedex.data.network.model.PokemonListItem
import com.eve.pokedex.databinding.ActivityDetailBinding
import com.eve.pokedex.view.detail.viewModel.DetailViewModel
import com.eve.pokedex.view.detail.viewModel.DetailViewModelFactory
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*

@ExperimentalCoroutinesApi
class DetailActivity : AppCompatActivity(), KodeinAware {
    override val kodein by kodein()
    private val factory: DetailViewModelFactory by instance()
    private lateinit var binding: ActivityDetailBinding
    private lateinit var viewModel: DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        viewModel = ViewModelProvider(this, factory).get(DetailViewModel::class.java)

        val pokemonName = intent.getStringExtra("name")!!
        viewModel.getPokemonDetail(pokemonName)
        binding.name.text = pokemonName.toUpperCase(Locale.US)


        binding.fav.setImageResource(if (UserData.favList.contains(pokemonName)) R.drawable.ic_favorite_enable else R.drawable.ic_favorite_disable)
        binding.fav.setOnClickListener {
            viewModel.setFavorite(pokemonName)
            binding.fav.setImageResource(if (UserData.favList.contains(pokemonName)) R.drawable.ic_favorite_enable else R.drawable.ic_favorite_disable)
        }

        lifecycleScope.launchWhenCreated {
            viewModel.pokemonDetailState.collect { it ->
                when (it) {
                    is DetailViewModel.PokemonDetailState.SUCCESS -> {
                        SvgLoader.pluck().with(this@DetailActivity)
                            .load(
                                it.pokemonDetail.sprites.other.dream_world.front_default,
                                binding.pokemonImg
                            )
                        val abilityBuilder = StringBuilder()
                        it.pokemonDetail.abilities.forEach {
                            abilityBuilder.append("• ".plus(it.ability.name.plus("\n")))
                        }
                        binding.abilities.text = abilityBuilder.toString()

                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        SvgLoader.pluck().close()
    }

}