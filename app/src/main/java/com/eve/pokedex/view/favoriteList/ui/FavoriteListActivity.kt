package com.eve.pokedex.view.favoriteList.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.eve.pokedex.R
import com.eve.pokedex.data.db.UserData
import com.eve.pokedex.data.network.model.PokemonListItem
import com.eve.pokedex.data.utils.RecyclerViewOnClick
import com.eve.pokedex.databinding.ActivityFavoriteBinding
import com.eve.pokedex.view.detail.ui.DetailActivity
import com.eve.pokedex.view.favoriteList.adapter.FavPokemonAdapter
import com.eve.pokedex.view.favoriteList.viewModel.FavoriteListViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein

@ExperimentalCoroutinesApi
class FavoriteListActivity : AppCompatActivity(), KodeinAware, RecyclerViewOnClick {
    override val kodein by kodein()
    private lateinit var binding: ActivityFavoriteBinding
    private lateinit var viewModel: FavoriteListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_favorite)
        viewModel = ViewModelProvider(this).get(FavoriteListViewModel::class.java)

        binding.recyclerView.also {
            it.layoutManager =
                LinearLayoutManager(this@FavoriteListActivity)
            it.adapter =
                FavPokemonAdapter(UserData.getFavList(), this@FavoriteListActivity)
        }
    }

    override fun onRecyclerViewClick(view: View, data: Any) {
        data as PokemonListItem
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra("name", data.name)
        startActivity(intent)
    }
}