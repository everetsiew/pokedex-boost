package com.eve.pokedex.data.network

import com.eve.pokedex.data.network.model.PokemonDetailResponse
import com.eve.pokedex.data.network.model.PokemonListResponse
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NetworkHelper {
    companion object {
        operator fun invoke(
            networkConnectionInterceptor: NetworkInterceptor
        ): NetworkHelper {
            val okkHttpclient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okkHttpclient)
                .baseUrl("https://pokeapi.co")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(NetworkHelper::class.java)
        }
    }

    @GET("/api/v2/pokemon")
    suspend fun getPokemonList(
        @Query("limit") limit: Int
    ): Response<PokemonListResponse>

    @GET("/api/v2/pokemon/{name}")
    suspend fun getPokemonDetail(
        @Path("name") name: String
    ): Response<PokemonDetailResponse>
}