package com.eve.pokedex.data.network.model

data class PokemonListResponse(val count: Int, val results: List<PokemonListItem>) {


}