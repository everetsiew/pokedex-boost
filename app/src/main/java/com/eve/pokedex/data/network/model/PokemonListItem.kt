package com.eve.pokedex.data.network.model

data class PokemonListItem(val name: String)