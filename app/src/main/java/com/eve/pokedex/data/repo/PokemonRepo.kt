package com.eve.pokedex.data.repo

import com.eve.pokedex.data.network.NetworkHelper
import com.eve.pokedex.data.network.NetworkResponse
import com.eve.pokedex.data.network.model.PokemonDetailResponse
import com.eve.pokedex.data.network.model.PokemonListResponse

class PokemonRepo(private val api: NetworkHelper) : NetworkResponse() {

    suspend fun getPokemonList(limit: Int): PokemonListResponse {
        return onCallApi { api.getPokemonList(limit) }
    }

    suspend fun getPokemonDetail(name: String): PokemonDetailResponse {
        return onCallApi { api.getPokemonDetail(name) }
    }
}