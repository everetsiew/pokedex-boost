package com.eve.pokedex.data.db

import com.eve.pokedex.data.network.model.PokemonListItem

object UserData {
    val favList = ArrayList<String>()

    fun getFavList(): List<PokemonListItem> {
        val modelList = ArrayList<PokemonListItem>()
        favList.forEach {
            modelList.add(PokemonListItem(it))
        }
        return modelList
    }
}