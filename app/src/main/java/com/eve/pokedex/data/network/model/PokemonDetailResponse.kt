package com.eve.pokedex.data.network.model

data class PokemonDetailResponse(
    val sprites: SpritesData,
    val abilities: List<AbilitiesDataList>
) {
    inner class SpritesData(val other: SpritesOther)
    inner class SpritesOther(val dream_world: SpritesOtherDreamWorld)
    inner class SpritesOtherDreamWorld(val front_default: String)

    inner class AbilitiesDataList(
        val ability: AbilityData
    )

    inner class AbilityData(val name: String)
}
