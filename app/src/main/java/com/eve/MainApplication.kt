package com.eve

import android.app.Application
import com.eve.pokedex.data.network.NetworkHelper
import com.eve.pokedex.data.repo.PokemonRepo
import com.eve.pokedex.data.network.NetworkInterceptor
import com.eve.pokedex.view.detail.viewModel.DetailViewModelFactory
import com.eve.pokedex.view.main.viewModel.MainViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class MainApplication : Application(), KodeinAware {
    override val kodein = Kodein.lazy {
        import(androidXModule(this@MainApplication))

        bind() from singleton { NetworkInterceptor(instance()) }
        bind() from singleton { NetworkHelper(instance()) }
        bind() from singleton { PokemonRepo(instance()) }
        bind() from provider { MainViewModelFactory(instance()) }
        bind() from provider { DetailViewModelFactory(instance()) }
    }
}